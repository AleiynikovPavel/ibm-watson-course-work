package com.iot;

public final class ConfigBuilder {

    String orgId;
    String deviceId;
    String typeId;
    String token;
    String comPortName;

    private ConfigBuilder() {
    }

    public static ConfigBuilder aConfig() {
        return new ConfigBuilder();
    }

    public ConfigBuilder withOrgId(String orgId) {
        this.orgId = orgId;
        return this;
    }

    public ConfigBuilder withDeviceId(String deviceId) {
        this.deviceId = deviceId;
        return this;
    }

    public ConfigBuilder withTypeId(String typeId) {
        this.typeId = typeId;
        return this;
    }

    public ConfigBuilder withToken(String token) {
        this.token = token;
        return this;
    }

    public ConfigBuilder withComPortName(String comPortName) {
        this.comPortName = comPortName;
        return this;
    }

    public Config build() {
        Config config = new Config();
        config.setOrgId(orgId);
        config.setDeviceId(deviceId);
        config.setTypeId(typeId);
        config.setToken(token);
        config.setComPortName(comPortName);
        return config;
    }

}
