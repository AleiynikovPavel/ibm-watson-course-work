package com.iot;

import org.eclipse.paho.client.mqttv3.MqttAsyncClient;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class MQTTClient {


    public static final long DEFAULT_ACTION_TIMEOUT = 10 * 1000L;
    private static final Logger logger = LoggerFactory.getLogger(MQTTClient.class);
    private final String password;
    private MqttAsyncClient mqttAsyncClient;

    public MQTTClient(Config config, MqttCallback callback) throws MqttException {
        this.password = config.getToken();
        String url = "ssl://" + config.getOrgId() + ".messaging.internetofthings.ibmcloud.com:8883";
        String clientId = "d:" + config.getOrgId() + ":" + config.getTypeId() + ":" + config.getDeviceId();
        MemoryPersistence persistence = new MemoryPersistence();
        this.mqttAsyncClient = new MqttAsyncClient(url, clientId, persistence);
        this.mqttAsyncClient.setCallback(callback);
    }

    public void connect() throws MqttException {
        MqttConnectOptions connectOptions = new MqttConnectOptions();
        connectOptions.setUserName("use-token-auth");
        connectOptions.setPassword(password.toCharArray());
        connectOptions.setAutomaticReconnect(true);
        this.mqttAsyncClient.connect(connectOptions).waitForCompletion(DEFAULT_ACTION_TIMEOUT);
        this.mqttAsyncClient.subscribe("iot-2/cmd/+/fmt/json",
                0).waitForCompletion(DEFAULT_ACTION_TIMEOUT);
        logger.info("Connected to IBM");
    }

}
