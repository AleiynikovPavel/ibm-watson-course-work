package com.iot;

import com.fazecast.jSerialComm.SerialPort;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SerialPortReceiver {

    private static final Logger logger = LoggerFactory.getLogger(SerialPortReceiver.class);

    private final SerialPort comPort;

    public SerialPortReceiver(Config config) {
        this.comPort = SerialPort.getCommPort(config.comPortName);
    }

    public void writeColor(int r, int g, int b) {
        if (!comPort.isOpen()) {
            comPort.openPort();
        }
        byte[] data = new byte[]{(byte) r, (byte) g, (byte) b};
        comPort.writeBytes(data, 3);
        logger.info("Write to serial port: {}", data);
    }

    public void close() {
        comPort.closePort();
    }

}
