package com.iot;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {

    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) throws MqttException {
        Config config = ConfigBuilder.aConfig()
                .withOrgId("j8ms46")
                .withTypeId("Server")
                .withDeviceId("JavaServer")
                .withToken("demodemo")
                .withComPortName("ttyUSB0")
                .build();

        SerialPortReceiver serialPort = new SerialPortReceiver(config);
        MQTTClient mqttClient = new MQTTClient(config, new MqttCallback() {
            @Override
            public void connectionLost(Throwable cause) {

            }

            @Override
            public void messageArrived(String topic, MqttMessage message) {
                logger.info("Receive from MQTT: {}", message);
                JSONObject obj = new JSONObject(message.toString());
                serialPort.writeColor(obj.getJSONObject("d").getInt("r"),
                        obj.getJSONObject("d").getInt("g"),
                        obj.getJSONObject("d").getInt("b"));
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken token) {

            }
        });
        mqttClient.connect();
        while (true) {
        }
    }

}
