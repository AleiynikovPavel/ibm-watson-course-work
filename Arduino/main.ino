#define GRBLED_PIN_R 11
#define GRBLED_PIN_G 10
#define GRBLED_PIN_B 9

int rgbled_r=0, rgbled_g=0, rgbled_b=0;

void setup(){
  Serial.begin(9600);
  pinMode(GRBLED_PIN_R, OUTPUT);
  pinMode(GRBLED_PIN_G, OUTPUT);
  pinMode(GRBLED_PIN_B, OUTPUT);
}

void loop(){
  readColor();
  setColor();
}

void setColor() {
  analogWrite(GRBLED_PIN_R, 255 - rgbled_r);
  analogWrite(GRBLED_PIN_G, 255 - rgbled_g);
  analogWrite(GRBLED_PIN_B, 255 - rgbled_b);
}

void readColor() {
   while(Serial.available() < 3) {}
   rgbled_r = Serial.read();
   rgbled_g = Serial.read();
   rgbled_b = Serial.read();
   Serial.println("R "+String(rgbled_r, HEX));
   Serial.println("G "+String(rgbled_g, HEX));
   Serial.println("B "+String(rgbled_b, HEX));
}
